﻿from __future__ import print_function, division

import numpy as np
import random
import datetime
import time
import os
import re
import gc
import h5py
import joblib
from sklearn.model_selection import KFold
from keras import backend as K
from keras.layers import Dense, Input, GlobalMaxPooling1D, concatenate, Dropout
from keras.layers import Conv1D, MaxPooling1D, Embedding
from keras.models import Model

SEED = 369
random.seed(369)
np.random.seed(369)

MAX_SEQUENCE_LENGTH = 1000
MAX_NUM_WORDS = 20000
EMBEDDING_DIM = 300

def rmsle(a, p):
    assert(a.shape[0]==p.shape[0])
    n = float(a.shape[0])
    return np.power(np.sum(np.power(p-a, 2))/n, .5)

def trained_model_predict(X_train, y_train, X_cv, X_train_float, X_cv_float, bagging=1, y_cv=None):
    ps = []
    for b in range(bagging):
        float_inp = Input(shape=[X_train_float.shape[1]], name="float_inp")

        int_inp = Input(shape=(MAX_SEQUENCE_LENGTH,), dtype='int32', name="int_inp")
        embedded_sequences = Embedding(num_words,
                            EMBEDDING_DIM,
                            embeddings_initializer='lecun_uniform',
                            input_length=MAX_SEQUENCE_LENGTH,
                            trainable=True)(int_inp)
        x = Conv1D(128, 7, activation='relu')(embedded_sequences)
        x = MaxPooling1D(7)(x)
        x = Conv1D(128, 7, activation='relu')(x)
        x = MaxPooling1D(7)(x)
        x = Conv1D(128, 7, activation='relu')(x)
        x = GlobalMaxPooling1D()(x)
        main_l = concatenate([
              x, float_inp
        ])
        main_l = Dense(1024, activation='relu')(main_l)
        main_l = Dense(512, activation='relu')(main_l)
        main_l = Dense(128, activation='relu')(main_l)
        preds = Dense(1)(main_l)

        model = Model([int_inp, float_inp], preds)
        model.compile(loss='mean_squared_error',
                      optimizer='adam')

        for i in range(5):
            print ("epoch",i)
            batch_size = 2**(4+i)
            if not y_cv is None:
                model.fit(x={"int_inp":X_train, "float_inp":X_train_float},
                    y=y_train, validation_data=({"int_inp":X_cv, "float_inp":X_cv_float}, y_cv ),
                    batch_size=batch_size, epochs=1, verbose=2, shuffle=True)
            else:
                model.fit(x={"int_inp":X_train, "float_inp":X_train_float},
                    y=y_train, batch_size=batch_size, epochs=1, verbose=2, shuffle=True)

        pred = model.predict({"int_inp":X_cv, "float_inp":X_cv_float})[:, 0]
        ps.append( pred )
        K.clear_session()
        gc.collect()
    pred = np.mean( ps, axis=0 )
    return pred

if __name__ == "__main__":
    #---load data------------------------------------
    with h5py.File('input/nn_conv1d.hdf5', 'r') as data_file:
        all_y = data_file['all_y'].value
        all_ids = data_file["all_ids"].value
        all_X = data_file["data"].value
        all_X_float = data_file["all_X_float"].value
        num_words = data_file["num_words"].value
    print (all_X.shape)
    print ("Done data load")

    mask = ~np.isnan(all_y[:,0])

    bag_freq = 3
    X, X_test = all_X[mask,:], all_X[~mask,:]
    X_float, X_test_float = all_X_float[mask,:], all_X_float[~mask,:]
    ids, ids_test = all_ids[mask], all_ids[~mask]

    meta_predictions = []
    for ti in range(7):
        y = all_y[mask, ti]

        scores, results = [], []
        skf = KFold(n_splits=5, shuffle=True, random_state=45447)
        for k, (train_idx, test_idx) in enumerate(skf.split(X, y)):
            print ("fold "+str(k))
            
            y_train, y_cv = y[train_idx], y[test_idx]
            wo_nan = ~np.isnan(y_train)
            X_train, X_cv = X[train_idx], X[test_idx]
            X_train_float, X_cv_float = X_float[train_idx], X_float[test_idx]
            ids_train, ids_cv = all_ids[train_idx], all_ids[test_idx]
            y_train, X_train, ids_train = y_train[wo_nan], X_train[wo_nan], ids_train[wo_nan]
            X_train_float = X_train_float[wo_nan]
            
            pred = trained_model_predict( X_train, y_train, X_cv, X_train_float, X_cv_float, bagging=bag_freq, y_cv=y_cv )
            
            wo_nan = ~np.isnan(y_cv)
            score = rmsle(y_cv[wo_nan], pred[wo_nan])
            
            results.append( (ids_cv, pred) )
            print (pred)
            print (y_cv)
            
            scores.append(score)
            print ( score )
            #break
        print (np.mean(scores), np.std( scores))

        wo_nan = ~np.isnan(y)
        ywn, Xwn, idswn = y[wo_nan], X[wo_nan], ids[wo_nan]
        X_floatwn = X_float[wo_nan]

        pred = trained_model_predict( Xwn, ywn, X_test, X_floatwn, X_test_float, bagging=bag_freq )
        results.append( (ids_test, pred) )
        
        idx2p = {}
        for ids_test, pred_1 in results:
            for idx, p1 in zip(ids_test, pred_1):
                idx2p[idx] = float(p1)
        meta_predictions.append( [idx2p.get(j, np.nan) for j in all_ids] )
    meta_predictions = np.array(meta_predictions).T

    joblib.dump(meta_predictions, "input/nn_pred_conv1d.pkl")
    print ("Done!")