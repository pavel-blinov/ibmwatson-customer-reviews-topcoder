Use IBM Watson to Predict Customer Reviews @ Topcoder
First place solution, team "now_you_see"
Pavel Blinov (blinoff.pavel@gmail.com), Data Scientist at Yandex, Moscow, Russia

For detailed description see "1st_place_documentation.pdf"

How to run

main.ipynb: Cell -> Run All

Upload stacking_*.pkl files to IBM Cloud Object Storage

xgb notebook (https://eu-gb.dataplatform.ibm.com/analytics/notebooks/v2/61a3acfb-5cb7-43e5-a8e6-abaec38242e1/view?access_token=e95210a127c06b318ed35108c6aff058022011ded742398e382a92939723950e) on IBM Watson Studio: Cell -> Run All