﻿from __future__ import print_function, division

import numpy as np
import pandas as pd
import random
import datetime
import time
import os
import re
import gc
import joblib
from sklearn import model_selection

from xgboost import XGBRegressor
import lightgbm as lgbm
import catboost

SEED = 369
random.seed(369)
np.random.seed(369)

def rmsle(a, p):
    assert(a.shape[0]==p.shape[0])
    n = float(a.shape[0])
    return np.power(np.sum(np.power(p-a, 2))/n, .5)

n_round, params = (450, {#lgbm params
    'task': 'train','boosting_type': 'gbdt','objective': 'regression','metric': 'rmse','verbose': 50,
    'num_leaves': 31,'learning_rate': 0.1,'feature_fraction': 0.9,'bagging_fraction': 0.8,'bagging_freq': 1 #5
})

def fit_regressor(r_name, X_train, y_train, X_cv=None, y_cv=None):
    if r_name=="lgbm":
        train_dset = lgbm.Dataset(X_train, y_train, silent=True)
        if X_cv is None:
            clf = lgbm.train(params, train_dset, valid_sets=[train_dset], valid_names=["train"],
                             num_boost_round=n_round, verbose_eval=1000)
        else:
            cv_dset = lgbm.Dataset(X_cv, y_cv, silent=True)
            clf = lgbm.train(params, train_dset, valid_sets=[train_dset, cv_dset], valid_names=["train", "cv"],
                             num_boost_round=n_round, verbose_eval=1000)
    elif r_name=="catboost":
        clf = catboost.CatBoostRegressor(random_seed=369)
        clf = clf.fit(X_train, y_train, logging_level="Silent")
    elif r_name=="xgb":
        clf = XGBRegressor(n_jobs=16, n_estimators=200, max_depth=8,
                           colsample_bytree=.95, min_child_weight=90, min_child_samples=.3)
        if X_cv is None:
            clf.fit(X_train, y_train, eval_set=[(X_train, y_train)], verbose=False)
        else:
            clf.fit(X_train, y_train, eval_set=[(X_train, y_train), (X_cv, y_cv)], verbose=False)
    return clf

def fit(r_name):
    final_scores = []
    results = []
    test_outs = []
    curr_y = y[:,0]
    bag_pred = []
    for fold, (train_indices, test_indices) in enumerate(skf.split(X, curr_y)):
        scores = []
        y_cvs, cv_predictions, test_predictions, fold_ids = [], [], [], []
        for curr_i, name in enumerate(target_names):
            curr_y = y[:,curr_i]
            X_train, X_cv = X[train_indices, :], X[test_indices, :]
            y_train, y_cv = curr_y[train_indices], curr_y[test_indices]
            ids_train, ids_cv = ids[train_indices], ids[test_indices]

            mtrain, mcv = ~np.isnan(y_train), ~np.isnan(y_cv)
            X_train = X_train[mtrain]
            y_train = y_train[mtrain]
            ids_train = ids_train[mtrain]

            ### train regressor specified by r_name
            clf = fit_regressor(r_name, X_train, y_train, X_cv, y_cv)
            pred = clf.predict(X_cv)
            p_test = clf.predict(X_test)

            fold_ids.append(ids_cv)
            y_cvs.append(y_cv)
            cv_predictions.append(pred)
            test_predictions.append(p_test)

            mcv = ~np.isnan(y_cv)
            y_cv = y_cv[mcv]
            pred = pred[mcv]

            if curr_i==0:
                pred = np.clip(pred, 20, 100)
                results.append(np.array([ids_cv, y_cv, pred]).T)
            else:
                pred = np.clip(pred, 2, 10)
            pred = np.round(pred, decimals=2)

            score = rmsle(y_cv, pred)
            scores.append(score)
            print (score)
            #break
        test_predictions = np.array(test_predictions).T
        cv_predictions = np.array(cv_predictions).T
        y_cvs = np.array(y_cvs).T
        fold_ids = fold_ids[0].reshape((-1,1))

        bag_pred.append([test_predictions, cv_predictions, y_cvs, fold_ids])
        score = np.max(20-np.sum(scores), 0)*1000000
        final_scores.append(score)
        print ("fold", fold, score)
        print()
    print (r_name, np.mean(final_scores), np.std(final_scores))
    results = np.vstack(results)
    
    y_cvs, cv_predictions, test_predictions, fold_ids = [], [], [], []
    for curr_i, name in enumerate(target_names):
        curr_y = y[:,curr_i]

        mtrain = ~np.isnan(curr_y)
        X_train = X[mtrain]
        y_train = curr_y[mtrain]
        ids_train = ids[mtrain]

        clf = fit_regressor(r_name, X_train, y_train)
        p_test = clf.predict(X_test)
        
        test_predictions.append(p_test)

    test_predictions = np.array(test_predictions).T
    bag_pred.append([test_predictions, cv_predictions, y_cvs, fold_ids])
    return bag_pred

if __name__ == "__main__":
    target_names = ["review_scores_rating","review_scores_accuracy","review_scores_cleanliness",
                    "review_scores_checkin","review_scores_communication","review_scores_location","review_scores_value"]

    #---load data------------------------------------
    all_X = joblib.load("input/all_X.pkl")
    num_train = pd.read_csv("input/train.csv", encoding="utf-8").shape[0]
    df = pd.concat([pd.read_csv("input/train.csv", encoding="utf-8"),
                    pd.read_csv("input/test.csv", encoding="utf-8", header=None, names=["listing_id"])], sort=False)
    all_ids = df.listing_id.values
    all_y = df.review_scores_rating.values
    #---load data------------------------------------
    model_names = ["lgbm", "catboost", "xgb"]
    number_of_folds = 5
    skf = model_selection.KFold(n_splits=number_of_folds, shuffle=True, random_state=SEED)
    
    group_feature_count = { #feature sets for different groups
        'all': 538, # all avaliable features
        'count_fast': 514, # simple count and fastText features
        'count_fast_linear': 524 # the same and lineal meta features
    }

    for group_name in group_feature_count:
        num_features = group_feature_count[group_name]
        print (group_name, num_features)
        all_y = df.iloc[:,1:].values
        
        mask = ~np.isnan(all_y[:,0]) #train / test mask
        X = all_X[mask, :num_features]
        print (X.shape)
        y = all_y[mask, :num_features]
        print (y.shape)
        ids = all_ids[mask]
        
        X_test = all_X[~mask, :num_features]
        print (X_test.shape)
        ids_test = all_ids[~mask]
        print (ids_test.shape)
        
        for j, r_name in enumerate(model_names):
            out_file_name = "input/l2_pred_"+group_name+"_"+r_name+".pkl"
            if not os.path.exists(out_file_name): #there is no pkl dump for this features set and model, let's do it
                preds = fit(r_name) #contains tuple of (test_predictions, cv_predictions, y_cvs, fold_ids) for each fold
                if group_name=="all" and r_name=="catboost": #once save ids and labels
                    y_cvs = np.vstack( (i[2] for i in preds[:number_of_folds]) )
                    joblib.dump(y_cvs, "input/l2_pred_labels.pkl")
                    fold_ids = np.vstack( (i[3] for i in preds[:number_of_folds]) )
                    joblib.dump(fold_ids, "input/l2_pred_ids.pkl")
                train_part = np.vstack( (i[1] for i in preds[:number_of_folds]) )
                test_part = np.mean([i[0] for i in preds], axis=0)
                
                l2_pred = np.vstack( (train_part, test_part) )
                print (l2_pred.shape)
                joblib.dump(l2_pred, out_file_name)
    print ("Done!")