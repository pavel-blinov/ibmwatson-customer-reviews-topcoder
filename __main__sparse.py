﻿from __future__ import print_function, division

import numpy as np
from scipy import sparse
import random
import datetime
import time
import os
import re
import gc
import h5py
import joblib
import keras as ks
from keras.models import Model
from keras.layers import Input, Dense
from keras import backend as K
from sklearn.model_selection import KFold

SEED = 369
random.seed(369)
np.random.seed(369)

def rmsle(a, p):
    assert(a.shape[0]==p.shape[0])
    n = float(a.shape[0])
    return np.power(np.sum(np.power(p-a, 2))/n, .5)

def trained_model_predict(X_train, y_train, X_cv, bagging=1):
    y_train = np.log1p( y_train )

    ps = []
    for b in range(bagging):
        model_in = ks.Input(shape=(X_train.shape[1],), dtype='float32', sparse=True)
        out = ks.layers.Dense(392, activation='relu')(model_in)
        out = ks.layers.Dropout(.5)(out)
        out = ks.layers.Dense(64, activation='relu')(out)
        out = ks.layers.Dropout(.5)(out)
        out = ks.layers.Dense(64, activation='relu')(out)
        out = ks.layers.Dense(1)(out)
        model = ks.Model(model_in, out)
        model.compile(loss='mean_squared_error', optimizer=ks.optimizers.Adam(lr=1e-3))
        for i in range(5):
            print ("epoch",i) #validation_data=(X_cv, y_cv),
            model.fit(x=X_train, y=y_train, batch_size=2**(4), epochs=1, verbose=2, shuffle=True)
        pred = model.predict(X_cv)[:, 0]
        pred = np.expm1( pred )
        ps.append( pred )
        K.clear_session()
        gc.collect()
    pred = np.mean( ps, axis=0 )
    return pred

if __name__ == "__main__":
    #---load data------------------------------------
    with h5py.File('input/nn_sparse.hdf5', 'r') as data_file:
        all_y = data_file['all_y'].value
        all_ids = data_file["all_ids"].value
        data = data_file["data"].value
        indices = data_file["indices"].value
        indptr = data_file["indptr"].value
        sh = (data_file["shape0"].value, data_file["shape1"].value)
        all_y = data_file["all_y"].value
        all_ids = data_file["all_ids"].value
    all_X = sparse.csr_matrix((data, indices, indptr), shape=sh)
    print (all_X.shape)
    print ("Done data load")

    mask = ~np.isnan(all_y[:,0])

    bag_freq = 3
    X, X_test = all_X[mask,:], all_X[~mask,:]
    ids, ids_test = all_ids[mask], all_ids[~mask]

    meta_predictions = []
    for ti in range(7):
        y = all_y[mask, ti]

        scores, results = [], []
        skf = KFold(n_splits=5, shuffle=True, random_state=4447)
        for k, (train_idx, test_idx) in enumerate(skf.split(X, y)):
            print ("fold "+str(k))
            
            y_train, y_cv = y[train_idx], y[test_idx]
            wo_nan = ~np.isnan(y_train)
            X_train, X_cv = X[train_idx], X[test_idx]
            ids_train, ids_cv = all_ids[train_idx], all_ids[test_idx]
            y_train, X_train, ids_train = y_train[wo_nan], X_train[wo_nan], ids_train[wo_nan]
            
            pred = trained_model_predict( X_train, y_train, X_cv, bagging=bag_freq )
            
            wo_nan = ~np.isnan(y_cv)
            score = rmsle(y_cv[wo_nan], pred[wo_nan])
            
            results.append( (ids_cv, pred) )
            print (pred)
            print (y_cv)
            
            scores.append(score)
            print ( score )
            #break
        print (np.mean(scores), np.std( scores))

        wo_nan = ~np.isnan(y)
        ywn, Xwn, idswn = y[wo_nan], X[wo_nan], ids[wo_nan]

        pred = trained_model_predict( Xwn, ywn, X_test, bagging=bag_freq )
        results.append( (ids_test, pred) )
        
        idx2p = {}
        for ids_test, pred_1 in results:
            for idx, p1 in zip(ids_test, pred_1):
                idx2p[idx] = float(p1)
        meta_predictions.append( [idx2p.get(j, np.nan) for j in all_ids] )
    meta_predictions = np.array(meta_predictions).T

    joblib.dump(meta_predictions, "input/nn_pred.pkl")
    print ("Done!")